#!/bin/bash

ov_should_skip_download=true
ov_should_backup_after_install=false
ov_native_package_installed=false


############################
##                        ##
##    useful tools        ##
##                        ##
############################


# Print echo in stderr 
# howto use : 
# echoerr hello world
function echoerr() 
{
	echo "$@" 1>&2; 
}


############################
##                        ##
##  installation scripts  ##
##                        ##
############################

function ov_hit_package
{
	# $1 is package name

	touch "$ov_target_folder_hit/$1.hit"
}

function ov_install_package
{
	# $1 is package name

	eval 'ov_current_package='$1
	eval 'ov_current_package_log_file=$ov_target_folder_log/'$1'.log'
	eval 'ov_current_package_hit_file=$ov_target_folder_hit/'$1'.hit'
	eval 'ov_current_package_archive_name=$ov_package_archive_'$1
	eval 'ov_current_package_url=$ov_package_url_'$1
	eval 'ov_current_package_additional_configure_flags=$ov_package_additional_configure_flags_'$1
	eval 'ov_current_package_backup=post-install-backup-'$1'.tar'

	eval 'ov_current_package_hook_download=$ov_package_download_hook_'$1
	eval 'ov_current_package_hook_download_pre=$ov_package_pre_download_hook_'$1
	eval 'ov_current_package_hook_download_post=$ov_package_post_download_hook_'$1
	eval 'ov_current_package_hook_checkout=$ov_package_checkout_hook_'$1
	eval 'ov_current_package_hook_checkout_pre=$ov_package_pre_checkout_hook_'$1
	eval 'ov_current_package_hook_checkout_post=$ov_package_post_checkout_hook_'$1
	eval 'ov_current_package_hook_uncompress=$ov_package_uncompress_hook_'$1
	eval 'ov_current_package_hook_uncompress_pre=$ov_package_pre_uncompress_hook_'$1
	eval 'ov_current_package_hook_uncompress_post=$ov_package_post_uncompress_hook_'$1
	eval 'ov_current_package_hook_configure=$ov_package_configure_hook_'$1
	eval 'ov_current_package_hook_configure_pre=$ov_package_pre_configure_hook_'$1
	eval 'ov_current_package_hook_configure_post=$ov_package_post_configure_hook_'$1
	eval 'ov_current_package_hook_make=$ov_package_make_hook_'$1
	eval 'ov_current_package_hook_make_pre=$ov_package_pre_make_hook_'$1
	eval 'ov_current_package_hook_make_post=$ov_package_post_make_hook_'$1
	eval 'ov_current_package_hook_install=$ov_package_install_hook_'$1
	eval 'ov_current_package_hook_install_pre=$ov_package_pre_install_hook_'$1
	eval 'ov_current_package_hook_install_post=$ov_package_post_install_hook_'$1

	ov_current_operation_result=0
	ov_current_operation_hook_result=0
	ov_current_operation_hook_pre_result=0
	ov_current_operation_hook_post_result=0

	if [ ! -e "$ov_current_package_hit_file" ]; then

		echo "Trying to install package [$ov_current_package]..."

		# goes to temporary folder
		pushd "$ov_target_folder_tmp" >> $ov_current_package_log_file 2>&1

		# empties temporary folder
		rm -rf *

		# resets log file
		rm -f $ov_current_package_log_file 2> /dev/null
		touch $ov_current_package_log_file

		# downloads package archive when needed
		echo "  Downloading [$ov_current_package_url] as [$ov_current_package_archive_name]..."
		if [[ "$ov_should_skip_download" == "true" && -e "$ov_target_folder_arch/$ov_current_package_archive_name" && ! "$ov_current_package_archive_name" == "" ]]; then
			echo "  Downloading [$ov_current_package_url] succeeded... (cached)"
		else
			if [ ! "$ov_current_package_hook_download" == "" ]; then
				# completely replace download phase with hook
				`eval $ov_current_package_hook_download >> $ov_current_package_log_file 2>&1`
				ov_current_operation_hook_result=$?
			else
				# executes download hook pre
				`eval $ov_current_package_hook_download_pre >> $ov_current_package_log_file 2>&1`
				ov_current_operation_hook_pre_result=$?
				# effectively download file(s)
				if [ ! "$ov_current_package_archive_name" == "" ]; then
					$ov_wget --retry-connrefused --random-wait -c -O "$ov_target_folder_arch/$ov_current_package_archive_name.tmp" $ov_current_package_url >> $ov_current_package_log_file 2>&1
					ov_current_operation_result=$?
					if [[ $ov_current_operation_result -eq 0 ]]; then
						mv -f "$ov_target_folder_arch/$ov_current_package_archive_name.tmp" "$ov_target_folder_arch/$ov_current_package_archive_name"
					fi;
				else
					$ov_svn checkout $ov_current_package_url >> $ov_current_package_log_file 2>&1
					ov_current_operation_result=$?
				fi;
				# executes download hook post
				`eval $ov_current_package_hook_download_post >> $ov_current_package_log_file 2>&1`
				ov_current_operation_hook_post_result=$?
			fi;
			if [[ $ov_current_operation_result -eq 0 && $ov_current_operation_hook_result -eq 0 && $ov_current_operation_hook_pre_result -eq 0 && $ov_current_operation_hook_post_result -eq 0 ]]; then
				echo "  Downloading [$ov_current_package_url] succeeded..."
			else
				echoerr "error:  Downloading [$ov_current_package_url] failed !"
				echo "  Log file can be found at [$ov_current_package_log_file] and ends with :"
				echo ""
				echo "`$ov_tail "$ov_current_package_log_file"`"
				exit 100
			fi;
		fi;

		# decompresses package archive in temporary folder
		if [ ! "$ov_current_package_archive_name" == "" ]; then
			echo "  Uncompressing [$ov_target_folder_arch/$ov_current_package_archive_name]..."
			if [ ! "$ov_current_package_hook_uncompress" == "" ]; then
				# completely replace uncompress phase with hook
				`eval $ov_current_package_hook_uncompress >> $ov_current_package_log_file 2>&1`
				ov_current_operation_hook_result=$?
			else
				# trying to figure out what kind of archive to decompress
				ov_package_type=`echo $ov_current_package_archive_name | $ov_sed '
					s/\(.*\)\.\(tar\.\)/\2/g
					s/\(.*\)\.\(tgz\)/\2/g
					s/\(.*\)\.\(zip\)/\2/g'`
				case $ov_package_type in
					"tgz")
						ov_decompress_command="$ov_tar xvfz"
						;;
					"tar.gz")
						ov_decompress_command="$ov_tar xvfz"
						;;
					"tar.bz2")
						ov_decompress_command="$ov_tar xvfj"
						;;
					"zip")
						ov_decompress_command="$ov_unzip"
						;;
					*)
						echo "  Uhandled file extension [$ov_target_folder_arch/$ov_current_package_archive_name], sorry (may you try hook system ?) !"
						exit 100
						;;
				esac;
				# executes uncompress hook pre
				`eval $ov_current_package_hook_uncompress_pre >> $ov_current_package_log_file 2>&1`
				ov_current_operation_hook_pre_result=$?
				# effectively uncompress file(s)
				$ov_decompress_command "$ov_target_folder_arch/$ov_current_package_archive_name" >> $ov_current_package_log_file 2>&1
				ov_current_operation_result=$?
				# executes uncompress hook post
				`eval $ov_current_package_hook_uncompress_post >> $ov_current_package_log_file 2>&1`
				ov_current_operation_hook_post_result=$?
			fi;
			if [[ $ov_current_operation_result -eq 0 && $ov_current_operation_hook_result -eq 0 && $ov_current_operation_hook_pre_result -eq 0 && $ov_current_operation_hook_post_result -eq 0 ]]; then
				echo "  Uncompressing [$ov_target_folder_arch/$ov_current_package_archive_name] succeeded..."
			else
				echoerr "error:  Uncompressing [$ov_target_folder_arch/$ov_current_package_archive_name] failed !"
				echo "  Log file can be found at [$ov_current_package_log_file] and ends with :"
				echo ""
				echo "`$ov_tail "$ov_current_package_log_file"`"
				exit 100
			fi;
		fi;

		# configures the package
		echo "  Configuring [$ov_current_package]..."
		if [ ! "$ov_current_package_hook_configure" == "" ]; then
			# completely replace configure phase with hook
			`eval $ov_current_package_hook_configure >> $ov_current_package_log_file 2>&1`
			ov_current_operation_hook_result=$?
		else
			# executes configure hook pre
			`eval $ov_current_package_hook_configure_pre >> $ov_current_package_log_file 2>&1`
			ov_current_operation_hook_pre_result=$?
			# effectively configure file
			pushd * >> $ov_current_package_log_file 2>&1
			./configure --prefix=$ov_target_folder $ov_current_package_additional_configure_flags CPPFLAGS="-I$ov_target_folder_include" LDFLAGS="-L$ov_target_folder_lib -L$ov_target_folder_lib64" >> $ov_current_package_log_file 2>&1
			ov_current_operation_result=$?
			popd >> $ov_current_package_log_file 2>&1
			# executes configure hook post
			`eval $ov_current_package_hook_configure_post >> $ov_current_package_log_file 2>&1`
			ov_current_operation_hook_post_result=$?
		fi;
		if [[ $ov_current_operation_result -eq 0 && $ov_current_operation_hook_result -eq 0 && $ov_current_operation_hook_pre_result -eq 0 && $ov_current_operation_hook_post_result -eq 0 ]]; then
			echo "  Configuring [$ov_current_package] succeeded..."
		else
			echoerr "error:  Configuring [$ov_current_package] failed !"
			echo "  Log file can be found at [$ov_current_package_log_file] and ends with :"
			echo ""
			echo "`$ov_tail "$ov_current_package_log_file"`"
			exit 100
		fi;

		# builds the package
		echo "  Building [$ov_current_package]..."
		if [ ! "$ov_current_package_hook_make" == "" ]; then
			# completely replace make phase with hook
			`eval $ov_current_package_hook_make >> $ov_current_package_log_file 2>&1`
			ov_current_operation_hook_result=$?
		else
			# executes make hook pre
			`eval $ov_current_package_hook_make_pre >> $ov_current_package_log_file 2>&1`
			ov_current_operation_hook_pre_result=$?
			# effectively make file
			pushd * >> $ov_current_package_log_file 2>&1
			$ov_make -j 2 >> $ov_current_package_log_file 2>&1
			ov_current_operation_result=$?
			popd >> $ov_current_package_log_file 2>&1
			# executes make hook post
			`eval $ov_current_package_hook_make_post >> $ov_current_package_log_file 2>&1`
			ov_current_operation_hook_post_result=$?
		fi;
		if [[ $ov_current_operation_result -eq 0 && $ov_current_operation_hook_result -eq 0 && $ov_current_operation_hook_pre_result -eq 0 && $ov_current_operation_hook_post_result -eq 0 ]]; then
			echo "  Building [$ov_current_package] succeeded..."
		else
			echoerr "error:  Building [$ov_current_package] failed !"
			echo "  Log file can be found at [$ov_current_package_log_file] and ends with :"
			echo ""
			echo "`$ov_tail "$ov_current_package_log_file"`"
			exit 100
		fi;

		# installs the package
		echo "  Installing [$ov_current_package]..."
		if [ ! "$ov_current_package_hook_install" == "" ]; then
			# completely replace install phase with hook
			`eval $ov_current_package_hook_install >> $ov_current_package_log_file 2>&1`
			ov_current_operation_hook_result=$?
		else
			# executes install hook pre
			`eval $ov_current_package_hook_install_pre >> $ov_current_package_log_file 2>&1`
			ov_current_operation_hook_pre_result=$?
			# effectively install file
			pushd * >> $ov_current_package_log_file 2>&1
			$ov_make install >> $ov_current_package_log_file 2>&1
			ov_current_operation_result=$?
			popd >> $ov_current_package_log_file 2>&1
			# executes install hook post
			`eval $ov_current_package_hook_install_post >> $ov_current_package_log_file 2>&1`
			ov_current_operation_hook_post_result=$?
		fi;
		if [[ $ov_current_operation_result -eq 0 && $ov_current_operation_hook_result -eq 0 && $ov_current_operation_hook_pre_result -eq 0 && $ov_current_operation_hook_post_result -eq 0 ]]; then
			echo "  Installing [$ov_current_package] succeeded..."
		else
			echoerr "error:  Installing [$ov_current_package] failed !"
			echo "  Log file can be found at [$ov_current_package_log_file] and ends with :"
			echo ""
			echo "`$ov_tail "$ov_current_package_log_file"`"
			exit 100
		fi;

		# hits this package
		touch "$ov_current_package_hit_file"

		# goes back to calling folder
		popd >> $ov_current_package_log_file 2>&1

		# backups target directory
		if [[ "$ov_should_backup_after_install" == "true" ]]; then
			echo "  Backuping target directory [$ov_current_package_backup]..."
			$ov_tar cf $ov_current_package_backup $ov_target_folder >> $ov_current_package_log_file 2>&1
			ov_current_operation_result=$?
			if [[ $ov_current_operation_result -eq 0 ]]; then
				echo "  Backuping target directory [$ov_current_package_backup] succeeded..."
			else
				echoerr "error:  Backuping target directory [$ov_current_package_backup] failed !"
				echo "  Log file can be found at [$ov_current_package_log_file] and ends with :"
v				echo ""
				echo "`$ov_tail "$ov_current_package_log_file"`"
				# exit
			fi;
		fi;

	else

		echo "Package [$ov_current_package] is already built, skipped..."

	fi;
}

######################################
##                                  ##
##  initializes some env variables  ##
##                                  ##
######################################

if [ "$1" == "" ]; then
	ov_target_folder=`pwd`/../dependencies
else
	mkdir $1 2> /dev/null
	pushd $1 2> /dev/null
	ov_target_folder=`pwd`
	popd 2> /dev/null
fi;


#########################
##                     ##
##  starts working...  ##
##                     ##
#########################

echo "Setting target folder to [$ov_target_folder]..."
echo ""

##############################################
##                                          ##
##  creates target directories when needed  ##
##                                          ##
##############################################

ov_target_folder_arch=$ov_target_folder/arch
ov_target_folder_tmp=$ov_target_folder/tmp
ov_target_folder_log=$ov_target_folder/log
ov_target_folder_hit=$ov_target_folder/hit
ov_target_folder_include=$ov_target_folder/include
ov_target_folder_bin=$ov_target_folder/bin
ov_target_folder_lib=$ov_target_folder/lib
ov_target_folder_lib64=$ov_target_folder/lib64
ov_target_folder_share=$ov_target_folder/share

echo "Creating target tree structure..."
mkdir "$ov_target_folder" 2> /dev/null
mkdir "$ov_target_folder_arch" 2> /dev/null
mkdir "$ov_target_folder_tmp" 2> /dev/null
mkdir "$ov_target_folder_log" 2> /dev/null
mkdir "$ov_target_folder_hit" 2> /dev/null
mkdir "$ov_target_folder_include" 2> /dev/null
mkdir "$ov_target_folder_bin" 2> /dev/null
mkdir "$ov_target_folder_lib" 2> /dev/null
mkdir "$ov_target_folder_share" 2> /dev/null
echo ""

####################################
##                                ##
##  checks current distribution   ##
##                                ##
####################################

version_greater() {
  expr "$1" = "`echo -e "$1\n$2" | sort -n | tail -n1`" 2>/dev/null
}

if [[ ! -e "$ov_target_folder_hit/no-native-packages.hit" ]]; then

	echo "Checking native dependencies..."

	[[ -z `grep -E 'Debian' /etc/issue 2> /dev/null` ]] ; is_debian=$?
        [[ -z `grep -E 'Ubuntu' /etc/lsb-release 2> /dev/null` ]] ; is_ubuntu=$? 
        [[ -z `grep -E 'LinuxMint' /etc/lsb-release 2> /dev/null` ]] ; is_mint=$? 
        [[ -z `grep -E 'Raspbian' /etc/os-release 2> /dev/null` ]] ; is_raspbian=$?

	if [[ $is_debian -eq 1 || $is_ubuntu -eq 1 || $is_raspbian -eq 1 || $is_mint -eq 1 ]]; then
			
		echo "  You are currently running a Debian/Ubuntu based distribution"

		ov_native_package_installed=true
		ov_native_package_log_file="$ov_target_folder_log/native-packages.log"
		ov_distro_version=`lsb_release -r -s 2>/dev/null`
		echo "  Found distro version $ov_distro_version ..."
		# Different distros and versions might need slightly different packages
		if [[ $is_ubuntu -eq 1 ]]; then
			if [[ `version_greater $ov_distro_version 16.04` == 1 ]]; then
				echo "  Ubuntu >= v16.04 detected, guessing specific set of packages..."
				ov_lib_ogre_dev=libogre-1.9-dev
				ov_lib_libboost_chrono_dev=libboost-chrono1.58-dev

			elif [[ `version_greater $ov_distro_version 14.04` == 1 ]]; then
				echo "  Ubuntu >= v14.04 detected, guessing specific set of packages..."
				ov_lib_ogre_dev=libogre-1.8-dev
				ov_lib_libboost_chrono_dev=libboost-chrono1.54-dev
			else
				echo "  Ubuntu seems older than v14.04 ..."
				ov_lib_ogre_dev=libogre-dev
				ov_lib_libboost_chrono_dev=
			fi;
		fi;
		if [[ $is_debian -eq 1 ]]; then
			if [[ `version_greater $ov_distro_version 8` == 1 ]]; then
				echo "  Debian >= v8 detected, guessing specific set of packages..."
				# although 1.9 is available, it doesn't seem to play well together with cegui, so we use 1.8 ...
				ov_lib_ogre_dev=libogre-1.8-dev
				ov_lib_libboost_chrono_dev=libboost-chrono1.55-dev
			else
				echo "  Debian seems older than v8 ..."
				ov_lib_ogre_dev=libogre-dev
				ov_lib_libboost_chrono_dev=
			fi
		fi;
		if [[ $is_mint -eq 1 ]]; then
			# hack the correct versions here...
			echo "  Mint detected, guessing specific set of packages..."
			ov_lib_ogre_dev=libogre-dev
			ov_lib_libboost_chrono_dev=
		fi
		if [[ $is_raspbian -eq 1 ]]; then
			# hack the correct versions here...
			echo "  Raspbian detected, guessing specific set of packages..."			
			ov_lib_ogre_dev=libogre-dev
			ov_lib_libboost_chrono_dev=
		fi
		ov_ubuntu_packages="wget doxygen make automake autoconf cmake unzip gcc g++ libgtk2.0-dev libglade2-dev gfortran libgsl0-dev libexpat1-dev libreadline-dev libzzip-dev libtool libxaw7-dev libpcre3-dev libfreeimage-dev libglu1-mesa-dev libalut-dev libvorbis-dev libncurses5-dev python-dev python-numpy libeigen3-dev $ov_lib_ogre_dev libcegui-mk2-dev libois-dev libboost-dev libboost-thread-dev liblua5.1-0-dev libboost-regex-dev libboost-filesystem-dev $ov_lib_libboost_chrono_dev libitpp-dev sqlite libsqlite0-dev libfftw3-dev"
			
		for package in $ov_ubuntu_packages; do
			ov_dpkg_output=`dpkg -l $package 2>&1 | grep "^ii"`
			ov_dpkg_retcode=$?

			if [[ $ov_dpkg_retcode != 0 ]]; then
				ov_native_package_installed=false
			fi;
			if [[ "$ov_dpkg_output" == "" ]]; then
				ov_native_package_installed=false
			fi;
		done;

		if [[ $ov_native_package_installed == false ]]; then
			echo ""
			echo "  Root access is required to install needed packages."
			echo ""

			echo "  Installing native packages on background..."

			sudo -k # revokes root privilege
			sudo apt-get --assume-yes install $ov_ubuntu_packages > $ov_native_package_log_file 2>&1
			ov_apt_get_output=$?
			sudo -k # revokes root privilege
			if [[ $ov_apt_get_output == 100 ]]; then
				
				echoerr "error: error in native package installation"
				cat "$ov_native_package_log_file"
				exit 100
			else	
				echo "  Installing native packages on background... done."
				ov_should_hit_native=true
				ov_native_package_installed=true
			fi;

		else
			echo "  Natively available packages seem to have been installed already."
		fi;
		
		# these sometimes non-native packages are available on Ubuntu, so no need to compile these
		ov_hit_package "itpp_external"
		ov_hit_package "itpp"

	fi;

	if [[ -e /etc/fedora-release ]]; then
		ov_native_package_installed=true
		ov_native_package_log_file="$ov_target_folder_log/native-packages.log"
		ov_fedora_packages="wget doxygen make automake autoconf cmake unzip gcc gcc-c++ gtk2-devel libglade2-devel gcc-gfortran expat-devel zziplib-devel libtool libXaw-devel python-devel numpy pcre-devel readline-devel freeimage-devel mesa-libGLU-devel freealut-devel libvorbis-devel bzip2 eigen3-devel ogre-devel ois-devel cegui-devel boost-regex boost boost-filesystem boost-math boost-thread lua-devel freetype sqlite sqlite-devel fftw-devel"

		echo "  You are currently running Fedora"
		echo ""
		echo "  Root access is required to install needed packages. "
		echo ""

		echo "  Installing native packages on background..."

		su -c "yum -y install $ov_fedora_packages " 2>&1 | tee $ov_native_package_log_file

		echo "  Installing native packages on background... done."
			
	fi;

	echo ""

fi;

####################################
##                                ##
##  checks supposed dependencies  ##
##                                ##
####################################

echo "Checking software script dependencies..."
ov_sh=`which sh`
ov_svn=`which svn`
ov_wget=`which wget`
ov_tar=`which tar`
ov_gzip=`which gzip`
ov_bzip2=`which bzip2`
ov_unzip=`which unzip`
ov_make=`which make`
ov_gcc=`which gcc`
ov_gpp=`which g++`
ov_sed=`which sed`
ov_find=`which find`
ov_uname=`which uname`
ov_true=`which true`
ov_false=`which false`
ov_tail=`which tail`
ov_automake=`which automake`
ov_autoconf=`which autoconf`

ov_g77=`which g77`
ov_gfortran=`which gfortran`
ov_g77_gfortran=""
ov_lib_g77_gfortran=""
if [ ! "$ov_gfortran" == "" ]; then
	ov_g77_gfortran="$ov_gfortran"
	ov_lib_g77_gfortran="gfortran"
else
	if [ ! "$ov_g77" == "" ]; then
		ov_g77_gfortran="$ov_g77"
		ov_lib_g77_gfortran="g2c"
	fi;
fi;

echo "  Using as sh           : [$ov_sh]..."
echo "  Using as subversion   : [$ov_svn]..."
echo "  Using as wget         : [$ov_wget]..."
echo "  Using as tar          : [$ov_tar]..."
echo "  Using as gzip         : [$ov_gzip]..."
echo "  Using as bzip2        : [$ov_bzip2]..."
echo "  Using as unnzip       : [$ov_unzip]..."
echo "  Using as make         : [$ov_make]..."
echo "  Using as gcc          : [$ov_gcc]..."
echo "  Using as g++          : [$ov_gpp]..."
echo "  Using as g77/gfortran : [$ov_g77_gfortran]..."
echo "  Using as sed          : [$ov_sed]..."
echo "  Using as find         : [$ov_find]..."
echo "  Using as uname        : [$ov_uname]..."
echo "  Using as true         : [$ov_true]..."
echo "  Using as false        : [$ov_false]..."
echo "  Using as tail         : [$ov_tail]..."
echo "  Using as automake     : [$ov_automake]..."
echo "  Using as autoconf     : [$ov_autoconf]..."
echo ""

ov_machine=`$ov_uname -m`

#####################################
##                                 ##
##  installation script execution  ##
##                                 ##
#####################################


##############################################
##                                          ##
##  initializes some environment variables  ##
##                   urls                   ##
##                                          ##
##############################################

# Computation and signal processing packages
ov_package_url_itpp_external=http://openvibe.inria.fr/dependencies/linux-x86/itpp-external-3.0.0.tar.bz2
ov_package_url_itpp=http://openvibe.inria.fr/dependencies/linux-x86/itpp-4.0.7.tar.bz2

# ov_package_url_vrpn=ftp://ftp.cs.unc.edu/pub/packages/GRIP/vrpn/old_versions/vrpn_07_26.zip
ov_package_url_vrpn=http://openvibe.inria.fr/dependencies/linux-x86/vrpn_07_31-ov.zip

# LabStreamingLayer
ov_package_url_liblsl=http://openvibe.inria.fr/dependencies/linux-x86/liblsl-1.04.ov1-src.tar.bz2

##############################################
##                                          ##
##  initializes some environment variables  ##
##              archive names               ##
##                                          ##
##############################################

# Computation and signal processing packages
ov_package_archive_itpp_external=`echo "$ov_package_url_itpp_external" | $ov_sed 's#.*/##g'`
ov_package_archive_itpp=`echo "$ov_package_url_itpp" | $ov_sed 's#.*/##g'`

# GFX related packages
ov_package_archive_vrpn=`echo "$ov_package_url_vrpn" | $ov_sed 's#.*/##g'`

# LabStreamingLayer
ov_package_archive_liblsl=`echo "$ov_package_url_liblsl" | $ov_sed 's#.*/##g'`

###########################################
##                                       ##
##  several hooks for specific archives  ##
##                                       ##
###########################################


ov_package_configure_hook_itpp_external='
	pushd * &&
	$ov_sed -i "s/_EXT_ETIME/_INT_ETIME/g" patches/lapack-3.1.1-autotools.patch &&
	$ov_sed -i "s/_EXT_ETIME/_INT_ETIME/g" src/lapack-lite-3.1.1/SRC/Makefile.in &&
	$ov_sed -i "s/_EXT_ETIME/_INT_ETIME/g" src/lapack-lite-3.1.1/SRC/Makefile.am &&
	./configure --prefix=$ov_target_folder &&
	popd'

ov_package_configure_hook_itpp='
	pushd * &&
	./configure --prefix=$ov_target_folder CPPFLAGS="-I$ov_target_folder_include" LDFLAGS="-L$ov_target_folder_lib -L$ov_target_folder_lib64" &&
	popd'

if [ "$ov_machine" == "x86_64" ]; then
ov_package_configure_hook_vrpn='
	$ov_sed -i "s/#HW_OS := pc_linux$/HW_OS := pc_linux64/g" vrpn/quat/Makefile &&
	$ov_sed -i "s/#HW_OS := pc_linux$/HW_OS := pc_linux64/g" vrpn/Makefile'
else
ov_package_configure_hook_vrpn='
	$ov_sed -i "s/#HW_OS := pc_linux$/HW_OS := pc_linux/g" vrpn/quat/Makefile &&
	$ov_sed -i "s/CC := gcc/CC := gcc -fPIC/g" vrpn/quat/Makefile &&
	$ov_sed -i "s/#HW_OS := pc_linux$/HW_OS := pc_linux/g" vrpn/Makefile &&
	$ov_sed -i "s/CC := gcc/CC := gcc -fPIC/g" vrpn/Makefile &&
	$ov_sed -i "s/CC := g++/CC := g++ -fPIC/g" vrpn/Makefile'
fi;
ov_package_make_hook_vrpn='
	pushd vrpn/quat &&
	make &&
	cd .. &&
	make &&
	popd'
if [ "$ov_machine" == "x86_64" ]; then
ov_package_install_hook_vrpn='
	chmod a-x vrpn/quat/*.h vrpn/*.h &&
	cp vrpn/quat/*.h vrpn/*.h ../include &&
	cp vrpn/quat/pc_linux64/*.a vrpn/pc_linux64/*.a ../lib'
else
ov_package_install_hook_vrpn='
	chmod a-x vrpn/quat/*.h vrpn/*.h &&
	cp vrpn/quat/*.h vrpn/*.h ../include &&
	cp vrpn/quat/pc_linux/*.a vrpn/pc_linux/*.a ../lib'
fi;

ov_package_configure_hook_liblsl='
	pushd * &&
	mkdir liblsl/build && 
	cd liblsl/build &&
	cmake .. &&
	popd'
ov_package_make_hook_liblsl='
	pushd * &&
	cd liblsl/build &&
	make &&
	popd'
ov_package_install_hook_liblsl='
	pushd * &&
	cp liblsl/build/src/liblsl.so $ov_target_folder_lib &&
	cp -R liblsl/include/* $ov_target_folder_include &&
	popd'
	


####################################
##                                ##
##  install non-native packages   ##
##                                ##
####################################

echo "Building packages that may not be available natively..."
echo ""

# Computation and signal processing packages
ov_install_package "itpp_external"
ov_install_package "itpp"

# Lab Streaming Layer, this is an optional dependence that enables LSL output plugin
ov_install_package "liblsl"

ov_install_package "vrpn"

echo "Install linux dependencies done"
